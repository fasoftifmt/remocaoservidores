// Initializes the `Bloqueios` service on path `/bloqueios`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { Bloqueios } from './bloqueios.class';
import createModel from '../database';

import hooks from './bloqueios.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'bloqueios': Bloqueios & ServiceAddons<any>;
  }
}

export default function (app: Application) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate
  };

  // Model.transaction(function(trx) {

  //   const books = [
  //     {title: 'Canterbury Tales'},
  //     {title: 'Moby Dick'},
  //     {title: 'Hamlet'}
  //   ];
  
  //   return trx
  //     .insert({name: 'Old Books'}, 'id')
  //     .into('catalogues')
  //     .then(function(ids) {
  //       books.forEach((book) => book.catalogue_id = ids[0]);
  //       return trx('books').insert(books);
  //     });
  // })
  // .then(function(inserts) {
  //   console.log(inserts.length + ' new books saved.');
  // })
  // .catch(function(error) {
  //   // If we get here, that means that neither the 'Old Books' catalogues insert,
  //   // nor any of the books inserts will have taken place.
  //   console.error(error);
  // });

  // Model.where()

  // Initialize our service with any options it requires
  app.use('/bloqueios', new Bloqueios(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('bloqueios');

  service.hooks(hooks);
}
