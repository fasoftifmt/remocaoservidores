import { Application } from './declarations.d';
import { ServiceAddons, Params } from '@feathersjs/feathers';
import { AuthenticationService, JWTStrategy, AuthenticationRequest, AuthenticationBaseStrategy } from '@feathersjs/authentication';
import { expressOauth } from '@feathersjs/authentication-oauth';
import { NotAuthenticated } from '@feathersjs/errors';

import createModel from './services/database';
import Knex = require('knex');

var ActiveDirectory = require('activedirectory');

var config = {
  // url: 'ldap://ldap.forumsys.com',
  // baseDN: 'dc=example,dc=com',
  // bindDN: 'cn=read-only-admin,dc=example,dc=com',
  // username: 'tesla@forumsys.com',
  // password: 'password'
}

var ad = new ActiveDirectory(config);

declare module './declarations' {
  interface ServiceTypes {
    'authentication': AuthenticationService & ServiceAddons<any>;
  }
}

class LocalStrategy extends AuthenticationBaseStrategy {
  Model: Knex<any, any[]>

  constructor(app: Application) {
    super()
    this.Model = createModel(app)
  }

  async authenticate(data: AuthenticationRequest, params: Params) {
    let user = await this.Model
      .select('id', 'matricula', 'nome', 'permissao')
      .from('usuarios')
      .where('matricula', '=', data.matricula)

    if (user.length > 0) {
      await this.comparePassword(data.matricula, data.senha)

      return {
        authentication: { strategy: 'local' },
        usuarios: user[0]
      };
    }

    else
      throw new NotAuthenticated('Usuário Inválido')
  }

  async comparePassword(matricula: string, password: string) {
    return new Promise((resolve, reject) => {
      resolve(true)
      // ad.authenticate(matricula, password, function (err: any, auth: any) {
      //   console.log(auth)
      //   if (err) {
      //     console.log('ERROR: ' + JSON.stringify(err));
      //     return;
      //   }

      //   if (auth) {
      //     resolve(true)
      //   }
      //   else {
      //     throw new NotAuthenticated('Senha Errada')
      //   }
      // });
    })
  }
}

export default function (app: any) {
  const authentication = new AuthenticationService(app);

  authentication.register('jwt', new JWTStrategy());
  authentication.register('local', new LocalStrategy(app));

  app.use('/login', authentication);
  app.configure(expressOauth());
}
