import { HookContext } from "@feathersjs/feathers";

export default {
  before: {
    all: [],
    find: [],
    get: [],
    create: [
      async (context: HookContext) => {
        context.data.data_criacao = new Date();
        context.data.data_atualizacao = new Date();
        return context;
      }
    ],
    update: [
      async (context: HookContext) => {
        context.data.data_atualizacao = new Date();
        return context;
      }
    ],
    patch: [
      async (context: HookContext) => {
        context.data.data_atualizacao = new Date();
        return context;
      }
    ],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
