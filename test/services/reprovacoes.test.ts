import assert from 'assert';
import app from '../../src/app';

describe('\'Reprovacoes\' service', () => {
  it('registered the service', () => {
    const service = app.service('reprovacoes');

    assert.ok(service, 'Registered the service');
  });
});
