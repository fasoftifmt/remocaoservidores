import assert from 'assert';
import app from '../../src/app';

describe('\'Campus\' service', () => {
  it('registered the service', () => {
    const service = app.service('campus');

    assert.ok(service, 'Registered the service');
  });
});
