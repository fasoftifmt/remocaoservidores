import assert from 'assert';
import app from '../../src/app';

describe('\'Bloqueios\' service', () => {
  it('registered the service', () => {
    const service = app.service('bloqueios');

    assert.ok(service, 'Registered the service');
  });
});
