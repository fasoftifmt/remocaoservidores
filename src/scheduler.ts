import { Application } from './declarations.d';
let nschedule = require('node-schedule')
import createModel from './services/database';
import { sendMail } from './services/sendmail';

let scheduler = {
    start: async function (app: Application) {
        nschedule.scheduleJob('21 * * * *', async function (fireDate: any) {
            const Model = createModel(app);

            let users = await Model
                .select('bloqueios.id', 'bloqueios.ativar', 'bloqueios.usuario', 'bloqueios.data_liberacao', 'usuarios.id', 'usuarios.email')
                .from('bloqueios').innerJoin('usuarios', 'bloqueios.usuario', 'usuarios.id')
                .where('bloqueios.ativar', '=', 1)
                .where('bloqueios.data_liberacao', '<=', new Date())
            

            await Model
                .from('bloqueios')
                .where('ativar', '=', 1)
                .where('data_liberacao', '<=', new Date())
                .update({ ativar: 0 })

            users.forEach(user => {
                sendMail(user.email, 'Desbloqueado para Remoção', 'Teste')
            })
        });
    }
}

export { scheduler }