// Initializes the `Inscricao` service on path `/inscricao`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { Inscricao } from './inscricao.class';
import createModel from '../database';

import hooks from './inscricao.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'inscricao': Inscricao & ServiceAddons<any>;
  }
}

export default function (app: Application) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/inscricao', new Inscricao(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('inscricao');

  service.hooks(hooks);
}
