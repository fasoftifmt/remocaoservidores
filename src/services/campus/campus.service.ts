// Initializes the `Campus` service on path `/campus`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { Campus } from './campus.class';
import createModel from '../database';

import hooks from './campus.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'campus': Campus & ServiceAddons<any>;
  }
}

export default function (app: Application) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/campus', new Campus(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('campus');

  service.hooks(hooks);
}
