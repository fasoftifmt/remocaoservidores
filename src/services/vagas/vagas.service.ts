// Initializes the `Vagas` service on path `/vagas`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { Vagas } from './vagas.class';
import createModel from '../database';

import hooks from './vagas.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'vagas': Vagas & ServiceAddons<any>;
  }
}

export default function (app: Application) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/vagas', new Vagas(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('vagas');

  service.hooks(hooks);
}
