import assert from 'assert';
import app from '../../src/app';

describe('\'Aprovacoes\' service', () => {
  it('registered the service', () => {
    const service = app.service('aprovacoes');

    assert.ok(service, 'Registered the service');
  });
});
